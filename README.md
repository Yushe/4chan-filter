# 4chan-filter

A List of text and md5 data filtered on 4chan's imageboards in JSON format.

Information is derived from testing the filter manually, online searches, threads on imageboards, issues created on this repo and much more.